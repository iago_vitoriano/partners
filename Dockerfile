FROM harbor.restoque.com.br/restoque/node/alpine-3.9/nodejs-10.16 AS build
EXPOSE 4200

RUN mkdir -p /home/node/app/node_modules \
  && chown -R node:node /home/node/app

WORKDIR /home/node/app
COPY . .

RUN echo '-------------------- DIR ------------------'
RUN ls -la
RUN echo '-------------------------------------------'

RUN echo y | npm install
RUN npm run build -- --output-path=./dist/out

FROM harbor.restoque.com.br/restoque/web-server/alpine-3.9/nginx-1.16:latest
LABEL maintainer="RESTOQUE S/A"

COPY --from=build /home/node/app /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

RUN touch /var/run/nginx.pid && \
  chown -R nginx:nginx /var/run/nginx.pid && \
  chown -R nginx:nginx /var/cache/nginx

USER nginx

CMD ["nginx", "-g", "daemon off;"]
