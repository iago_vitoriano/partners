const FirestoreBaseRepository = require('./FirestoreBaseRepository');
const db = require('../../../start/database');

class PostsRepository extends FirestoreBaseRepository {
  constructor() {
    super('partners', db);
  }

  async getAll(partnerId) {
    const query = this.collection.doc(partnerId).collection('posts');

    const snapshot = await query.get();

    if (snapshot.empty) return [];

    const documents = [];

    snapshot.forEach(doc => {
      documents.push({
        ...doc.data(),
        id: doc.id,
      });
    });

    return documents;
  }

  async create(partnerId, documentContent) {
    const newDocument = this.collection
      .doc(partnerId)
      .collection('posts')
      .doc();

    await newDocument.set(documentContent);

    return { id: newDocument.id };
  }

  async getById(partnerId, id) {
    const document = this.collection
      .doc(partnerId)
      .collection('posts')
      .doc(id);

    const documentSnapshot = await document.get();

    return documentSnapshot.data();
  }

  async updateOrCreateById(partnerId, id, documentContent) {
    const document = this.collection
      .doc(partnerId)
      .collection('posts')
      .doc(id);

    await document.set(documentContent, { merge: true });
  }

  async deleteById(partnerId, id) {
    const document = this.collection
      .doc(partnerId)
      .collection('posts')
      .doc(id);

    await document.delete();
  }
}

module.exports = new PostsRepository();
