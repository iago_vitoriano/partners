const FirestoreBaseRepository = require('./FirestoreBaseRepository');
const db = require('../../../start/database');

class PartnersRepository extends FirestoreBaseRepository {
  constructor() {
    super('partners', db);
  }
}

module.exports = new PartnersRepository();
