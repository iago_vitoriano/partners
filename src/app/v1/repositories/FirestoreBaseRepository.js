class FirestoreBaseRepository {
  /**
   * Create a FirestoreBaseRepository.
   * @param {string} collectionName - Collection name.
   * @param {Object} firestoreInstance - Firestore instance.
   *
   * @return {Object} Class instance.
   */
  constructor(collectionName, firestoreInstance) {
    this.firestore = firestoreInstance;
    this.collection = this.firestore.collection(collectionName);

    return this;
  }

  /**
   * Checks if the document exists.
   * @param {String} id - Document identification.
   *
   * @return {Promise<{boolean}>} Promise to document exists.
   */
  async exists(id) {
    const ref = this.collection.doc(id);
    const doc = await ref.get();

    return doc.exists;
  }

  /**
   * Insert document into collection.
   * @param {Object} documentContent - Document content.
   *
   * @return {Promise<{Object}>} Promise to document identification of created document.
   */
  async create(documentContent) {
    const newDocument = this.collection.doc();

    await newDocument.set(documentContent);

    return { id: newDocument.id };
  }

  /**
   * Insert multiple documents into collection.
   * @param {Object} documentContent - Object with multiple documents content in Sub Collections.
   *
   * @return {Promise<{Object}>} Promise to document identification of created document.
   */
  async bulkInsertSubCollection(documentContent) {
    const batch = this.batch();

    documentContent.forEach(document => {
      const doc = this.collection
        .doc(document.collectionId)
        .collection(document.subCollectionName)
        .doc(document.subCollectionId);

      batch.set(doc, document.data);
    });

    batch.commit();
  }

  /**
   * Create or update document.
   * @param {String} id - Document identification.
   * @param {Object} documentContent - Document content.
   *
   * @return {Promise<undefined>} Promise of update or create document.
   */
  async updateOrCreateById(id, documentContent) {
    const document = this.collection.doc(id);

    await document.set(documentContent, { merge: true });
  }

  /**
   * Get all documents in collection.
   *
   * @return {Promise<Object>} Promise to list of document content.
   */
  getAll(pending) {
    let query = this.collection;

    if (pending) query = query.where('active', '==', null);

    return query.get().then(snapshot => {
      if (snapshot.empty) return [];

      const documents = [];

      snapshot.forEach(doc => {
        documents.push({
          ...doc.data(),
          id: doc.id,
        });
      });
      return documents;
    });
  }

  /**
   * Get all documents in collection.
   *
   * @return {Promise<Object>} Promise to list of document content with subcollections.
   */
  async getAllWithSubCollections() {
    const documents = [];

    const snapshot = await this.collection.get();

    if (snapshot.empty) return [];

    for (const doc of snapshot.docs) {
      const subCollectionsIds = [];

      const subCollections = await this.collection
        .doc(doc.id)
        .listCollections();

      subCollections.forEach(subCollection => {
        subCollectionsIds.push(subCollection.id);
      });

      documents.push({
        id: doc.id,
        data: doc.data(),
        subCollections: subCollectionsIds,
      });
    }

    return documents;
  }

  /**
   * Get all documents in subcollection collection.
   * @param {String} documentId - Document identification.
   * @param {String} documentId - Document identification.
   *
   * @return {Promise<Object>} Promise to list of document content.
   */
  getAllSubCollection(documentId, subCollectionId) {
    return this.collection
      .doc(documentId)
      .collection(subCollectionId)
      .get()
      .then(snapshot => {
        const documents = [];

        snapshot.forEach(doc => {
          documents.push({ id: doc.id, data: doc.data() });
        });
        return documents;
      });
  }

  /**
   * Find document by id.
   * @param {String} id - Document identification.
   *
   * @return {Promise<Object>} Promise to document content.
   */
  async getById(id) {
    const document = this.collection.doc(id);

    const documentSnapshot = await document.get();

    return documentSnapshot.data();
  }

  /**
   * Find document by id.
   * @param {String} id - Document identification.
   *
   * @return {Promise<Object>} Promise to document content subcollection id's.
   */
  async getByIdWithSubCollections(id) {
    const subCollectionsIds = [];

    const snapshot = await this.collection.doc(id).get();

    if (snapshot.empty) return [];

    const subcollections = await this.collection.doc(id).listCollections();

    subcollections.forEach(subCollection => {
      subCollectionsIds.push(subCollection.id);
    });

    return {
      id: snapshot.id,
      data: snapshot.data(),
      subCollections: subCollectionsIds,
    };
  }

  /**
   * Delete document by id.
   * @param {string} id - Document identification.
   *
   * @return {Promise<boolean>} Promise to document deleted.
   */
  async deleteById(id) {
    const document = this.collection.doc(id);

    await document.delete();
  }

  /**
   * Create document if not exists.
   * @param {string} id - Document identification.
   * @param {Object} documentContent - Document content.
   *
   * @return {Promise<undefined>} Promise.
   */
  async createIfNotExists(id, documentContent) {
    const documentReference = this.collection.doc(id);

    const doc = await this.firestore.runTransaction(transaction =>
      transaction.get(documentReference),
    );

    if (doc.exists) return Promise.resolve();

    await doc.ref.set(documentContent);

    return undefined;
  }

  /**
   * Check if subDocumentId exists in document subcollection.
   * @param {string} documentId - Document identification of subcollection.
   * @param {string} subcollectionId - Subcollection identification.
   * @param {string} subdocumentId - Subdocument identification.
   * @param {Object} documentContent - Document content.
   *
   * @return {Promise<boolean>} Promise.
   */
  createOrUpdateInSubCollection(
    documentId,
    subcollectionName,
    subdocumentId,
    documentContent,
  ) {
    return this.collection
      .doc(documentId)
      .collection(subcollectionName)
      .doc(subdocumentId)
      .set(documentContent);
  }

  /**
   * Check if subDocumentId exists in document subcollection.
   * @param {string} documentId - Document identification of subcollection.
   * @param {string} subcollectionId - Subcollection identification.
   * @param {string} subdocumentId - Subdocument identification.
   *
   * @return {Promise<boolean>} Promise.
   */
  existsInSubCollection(documentId, subcollectionName, subdocumentId) {
    return this.collection
      .doc(documentId)
      .collection(subcollectionName)
      .doc(subdocumentId)
      .get()
      .then(item => item.exists);
  }

  /**
   * Check if subDocumentId exists in document subcollection.
   * @param {string} documentId - Document identification of subcollection.
   * @param {string} subcollectionId - Subcollection identification.
   * @param {string} subdocumentId - Subdocument identification.
   *
   * @return {Promise<boolean>} Promise.
   */
  deleteInSubCollection(documentId, subcollectionName, subdocumentId) {
    return this.collection
      .doc(documentId)
      .collection(subcollectionName)
      .doc(subdocumentId)
      .delete();
  }
}

module.exports = FirestoreBaseRepository;
