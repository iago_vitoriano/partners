const FirestoreBaseRepository = require('./FirestoreBaseRepository');
const db = require('../../../start/database');

class UsersRepository extends FirestoreBaseRepository {
  constructor() {
    super('admins', db);
  }

  getAll() {
    return this.collection
      .where('role', '==', 0)
      .get()
      .then(snapshot => {
        if (snapshot.empty) return [];

        const documents = [];

        snapshot.forEach(doc => {
          documents.push({
            ...doc.data(),
            id: doc.id,
          });
        });
        return documents;
      });
  }

  async getByKey(key, data) {
    const documents = [];
    const snapshot = await this.collection.where(key, '==', data).get();

    snapshot.forEach(document => {
      documents.push({
        id: document.id,
        ...document.data(),
      });
    });

    return documents;
  }
}

module.exports = new UsersRepository();
