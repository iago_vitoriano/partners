const Joi = require('@hapi/joi');

const createSchema = Joi.object().keys({
  name: Joi.string().required(),
  document: Joi.string().required(),
  email: Joi.string()
    .email()
    .required(),
  phone: Joi.string().required(),
  site: Joi.string().required(),
  coverPhoto: Joi.string()
    .uri()
    .required(),
  gallery: Joi.array().items(
    Joi.string()
      .uri()
      .allow(''),
  ),
  ngo: Joi.bool().required(),
  openingHours: Joi.string(),
  description: Joi.string(),
  address: Joi.object()
    .keys({
      state: Joi.string().required(),
      city: Joi.string().required(),
      neighborhood: Joi.string().required(),
      street: Joi.string().required(),
      streetNumber: Joi.number()
        .integer()
        .required(),
      zipcode: Joi.string().required(),
    })
    .required(),
  responsible: Joi.object()
    .keys({
      firstname: Joi.string().required(),
      lastname: Joi.string().required(),
      email: Joi.string().required(),
    })
    .required(),
});

module.exports = createSchema;
