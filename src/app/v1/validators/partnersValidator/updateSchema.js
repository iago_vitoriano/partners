const Joi = require('@hapi/joi');

const updateSchema = Joi.object().keys({
  name: Joi.string(),
  document: Joi.string(),
  email: Joi.string().email(),
  phone: Joi.string(),
  site: Joi.string(),
  description: Joi.string(),
  openingHours: Joi.string(),
  ngo: Joi.bool(),
  coverPhoto: Joi.string()
    .uri()
    .allow(''),
  gallery: Joi.array().items(
    Joi.string()
      .uri()
      .allow(''),
  ),
  address: Joi.object().keys({
    state: Joi.string().required(),
    city: Joi.string().required(),
    neighborhood: Joi.string().required(),
    street: Joi.string().required(),
    street_number: Joi.number()
      .integer()
      .required(),
    zipcode: Joi.string().required(),
  }),
  responsible: Joi.object().keys({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    email: Joi.string().required(),
  }),
  active: Joi.bool(),
});

module.exports = updateSchema;
