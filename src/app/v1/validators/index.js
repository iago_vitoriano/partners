const partnersValidator = require('./partnersValidator');
const postsValidator = require('./postsValidator');

module.exports = {
  partnersValidator,
  postsValidator,
};
