const Joi = require('@hapi/joi');

const updateSchema = Joi.object().keys({
  userId: Joi.string(),
  active: Joi.bool(),
  title: Joi.string(),
  image: Joi.string().uri(),
  content: Joi.string(),
  source: Joi.string()
    .uri()
    .allow(''),
  type: Joi.string().valid('ngo', 'public'),
});

module.exports = updateSchema;
