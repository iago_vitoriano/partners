const Joi = require('@hapi/joi');

const createSchema = Joi.object().keys({
  userId: Joi.string().required(),
  active: Joi.bool().required(),
  title: Joi.string().required(),
  image: Joi.string()
    .uri()
    .required(),
  content: Joi.string().required(),
  source: Joi.string()
    .uri()
    .allow(''),
  type: Joi.string()
    .valid('ngo', 'public')
    .required(),
});

module.exports = createSchema;
