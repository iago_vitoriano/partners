const httpStatusCodes = require('http-status-codes');

const postsService = require('../services/postsService');

/**
 * Resourceful controller for interacting with Posts
 */
class PostsController {
  /**
   * Show a list of all posts data.
   * GET posts
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async index(req, res) {
    const { partnerId } = req.params;

    const documents = await postsService.findAll(partnerId);

    return res.status(httpStatusCodes.OK).json(documents);
  }

  /**
   * Save a new posts data.
   * POST posts
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async store(req, res) {
    const { partnerId } = req.params;

    await postsService.create(partnerId, req.body);

    return res.status(httpStatusCodes.CREATED).send();
  }

  /**
   * Display a single posts data by id.
   * GET posts/:id
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async show(req, res) {
    const { partnerId, id } = req.params;

    const sellers = await postsService.findById(partnerId, id);

    return res.status(httpStatusCodes.OK).json(sellers);
  }

  /**
   * Update posts data by id.
   * PUT or PATCH posts/:id
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async update(req, res) {
    const { partnerId, id } = req.params;

    await postsService.updateById(partnerId, id, req.body);

    return res.status(httpStatusCodes.NO_CONTENT).send();
  }

  /**
   * Delete a posts data by id.
   * DELETE posts/:id
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async destroy(req, res) {
    const { partnerId, id } = req.params;

    await postsService.deleteById(partnerId, id);

    return res.status(httpStatusCodes.NO_CONTENT).send();
  }
}

module.exports = new PostsController();
