const httpStatusCodes = require('http-status-codes');

const partnersService = require('../services/partnersService');

/**
 * Resourceful controller for interacting with Partners
 */
class PartnersController {
  /**
   * Show a list of all partners data.
   * GET partners
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async index(req, res) {
    const { pending } = req.query;

    const documents = await partnersService.findAll(pending);

    return res.status(httpStatusCodes.OK).json(documents);
  }

  /**
   * Save a new partners data.
   * POST partners
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async store(req, res) {
    await partnersService.create(req.body);

    return res.status(httpStatusCodes.CREATED).send();
  }

  /**
   * Display a single partners data by id.
   * GET partners/:id
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async show(req, res) {
    const { id } = req.params;

    const sellers = await partnersService.findById(id);

    return res.status(httpStatusCodes.OK).json(sellers);
  }

  /**
   * Update partners data by id.
   * PUT or PATCH partners/:id
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async update(req, res) {
    const { id } = req.params;

    await partnersService.updateById(id, req.body);

    return res.status(httpStatusCodes.NO_CONTENT).send();
  }

  /**
   * Delete a partners data by id.
   * DELETE partners/:id
   *
   * @param {req} req - Express request object
   * @param {res} res - Express response object
   */
  async destroy(req, res) {
    const { id } = req.params;

    await partnersService.deleteById(id);

    return res.status(httpStatusCodes.NO_CONTENT).send();
  }
}

module.exports = new PartnersController();
