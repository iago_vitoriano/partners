const ValidationError = require('./ValidationError');

/**
 * @module validateJoi
 *
 * @function
 *
 * @param {Function} schema - Joi schema to validate
 * @param {String} property - Express request object attribute to be checked
 *
 * @return {validateJoi~middleware} - Express middleware
 */
module.exports = (schema, property) => {
  /**
   * middleware
   *
   * @param {Object} req - Express request object
   * @param {Object} res - Express response object
   * @param {Function} next - Express next middleware function
   *
   * @return {Object}
   */
  return (req, res, next) => {
    const validate = schema.validate(req[property], {
      abortEarly: false,
    });

    if (!validate.error) return next();

    throw new ValidationError(
      `${validate.error.details
        .map(errorDescription => errorDescription.message)
        .join('. ')}.`,
    );
  };
};
