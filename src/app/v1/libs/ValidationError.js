const httpStatus = require('http-status-codes');

/** Class representing a error of validation. */
class ValidationError extends Error {
  /**
   * Create a Validation Error object.
   * @param {String} message - Error message.
   *
   * @return {ValidationError} A ValidationError object.
   */
  constructor(message) {
    super();

    this.status = httpStatus.UNPROCESSABLE_ENTITY;
    this.message = message;

    return this;
  }
}

module.exports = ValidationError;
