const httpStatus = require('http-status-codes');

/** Class representing a error of validation. */
class InternalServerError extends Error {
  /**
   * Create a Validation Error object.
   * @param {String} message - Error message.
   *
   * @return {ValidationError} A ValidationError object.
   */
  constructor(message) {
    super();

    this.status = httpStatus.INTERNAL_SERVER_ERROR;
    this.message = message;

    return this;
  }
}

module.exports = InternalServerError;
