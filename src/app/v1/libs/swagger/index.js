const fs = require('fs');
const path = require('path');
const j2s = require('hapi-joi-to-swagger');
const yaml = require('json-to-pretty-yaml');

const directory = require('../directory');
const swaggerHelpers = require('./helpers');

const create = (validatorsPath, swaggerPath, version) => {
  directory.createDirectoryIfNotExists(swaggerPath);

  directory.getDirectories(validatorsPath).forEach(folder => {
    const validator = require(folder);
    const actions = Object.keys(validator);
    const folderName = swaggerHelpers.getFolderName(folder);
    const source = path.resolve(swaggerPath, folderName);

    directory.createDirectoryIfNotExists(source);

    actions.forEach(action => {
      const { swagger: joiToSwagger } = j2s(validator[action]);
      const targetPath = path.resolve(
        swaggerPath,
        folderName,
        `${action}.yaml`,
      );
      const defaultJsonSchema = swaggerHelpers.shiftSchemaIntoDefinitionWithName(
        folderName,
        version,
        action,
        joiToSwagger,
      );

      const yamlData = yaml.stringify(defaultJsonSchema);

      fs.writeFileSync(targetPath, yamlData);
    });
  });

  console.log('Documentos do Swagger gerados com sucesso!');
};

module.exports = create;
