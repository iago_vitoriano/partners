const shiftSchemaIntoDefinitionWithName = (
  entity,
  version = 'v1',
  name,
  schema,
) => {
  return { definitions: { [`${entity}_${name}_${version}`]: schema } };
};

const getFolderName = folder => folder.split('/')[folder.split('/').length - 1];

module.exports = {
  shiftSchemaIntoDefinitionWithName,
  getFolderName,
};
