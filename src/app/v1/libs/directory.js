/**
 * Base directory module to use inside helpers.
 *
 * Define module with base functions to manage directories using default file system module.
 *
 * @file   This file defines the directory management module
 * @author Iago Vitoriano
 * @since  2020.01.23
 */

/**
 * Module dependencies.
 */
const path = require('path');
const fs = require('fs');

/**
 * Check if sourcePath path is a directory
 *
 * @param {String} sourcePath
 * @return {Boolean}
 */
const isDirectory = sourcePath => {
  try {
    return fs.lstatSync(sourcePath).isDirectory();
  } catch (err) {
    return false;
  }
};

/**
 * Get all directories inside sourcePath path
 *
 * @param {String} sourcePath
 * @return {Array}
 */
const getDirectories = sourcePath =>
  fs
    .readdirSync(sourcePath)
    .map(name => path.join(sourcePath, name))
    .filter(isDirectory);

/**
 * Create directory path if not exists
 *
 * @param {String} sourcePath
 *
 */
const createDirectoryIfNotExists = sourcePath => {
  if (!fs.existsSync(sourcePath)) fs.mkdirSync(sourcePath, { recursive: true });
};

/**
 * Get all files inside sourcePath path
 *
 * @param {String} sourcePath
 * @return {Array}
 */
const getFiles = sourcePath => fs.readdirSync(sourcePath);

module.exports = {
  isDirectory,
  createDirectoryIfNotExists,
  getDirectories,
  getFiles,
};
