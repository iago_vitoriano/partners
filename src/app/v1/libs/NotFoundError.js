const httpStatus = require('http-status-codes');

class NotFoundError extends Error {
  constructor(message) {
    super();
    Error.captureStackTrace(this, NotFoundError);

    this.status = httpStatus.NOT_FOUND;
    this.message = message;

    return this;
  }
}

module.exports = NotFoundError;
