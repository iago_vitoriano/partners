const Youch = require('youch');

const InternalServerError = require('./InternalServerError');

/**
 * @module globalErrorHandler
 *
 * @function
 *
 * @param {Object} originalError - Express error object
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} _next - Express next middleware function
 *
 * @return {Object}
 */
module.exports = async (originalError, req, res, _next) => {
  let error = originalError;

  if (!error.status) error = new InternalServerError(originalError.message);

  if (process.env.NODE_ENV === 'production')
    return res.status(error.status).json({ message: error.message });

  const stackErrorDescription = await new Youch(originalError, req).toJSON();

  return res.status(error.status).json(stackErrorDescription);
};
