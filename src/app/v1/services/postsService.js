const moment = require('moment');
const admin = require('firebase-admin');

const postsRepository = require('../repositories/postsRepository');
const NotFoundError = require('../libs/NotFoundError');

class PostsService {
  async findAll(partnerId) {
    const documents = await postsRepository.getAll(partnerId);

    return documents.map(data => {
      return {
        ...data,
        createdAt: moment(data.createdAt._seconds * 1000).format('DD/MM/YYYY'),
        updatedAt: moment(data.updatedAt._seconds * 1000).format('DD/MM/YYYY'),
      };
    });
  }

  async create(partnerId, data) {
    await postsRepository.create(partnerId, {
      ...data,
      likes: 0,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });
  }

  async findById(partnerId, id) {
    const document = await postsRepository.getById(partnerId, id);

    if (!document) throw new NotFoundError('O documento não existe.');

    return {
      ...document,
      createdAt: moment(document.createdAt._seconds * 1000).format(
        'DD/MM/YYYY',
      ),
      updatedAt: moment(document.updatedAt._seconds * 1000).format(
        'DD/MM/YYYY',
      ),
    };
  }

  async updateById(partnerId, id, data) {
    const document = await postsRepository.getById(partnerId, id);

    if (!document) throw new NotFoundError('O documento não existe.');

    await postsRepository.updateOrCreateById(partnerId, id, {
      ...data,
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });
  }

  async deleteById(partnerId, id) {
    const document = await postsRepository.getById(partnerId, id);

    if (!document) throw new NotFoundError('O documento não existe.');

    postsRepository.deleteById(partnerId, id);
  }
}

module.exports = new PostsService();
