const moment = require('moment');
const admin = require('firebase-admin');

const partnersRepository = require('../repositories/partnersRepository');
const usersRepository = require('../repositories/usersRepository');
const NotFoundError = require('../libs/NotFoundError');

class PartnersService {
  async findAll(pending) {
    const documents = await partnersRepository.getAll(pending);

    return documents.map(data => {
      return {
        ...data,
        createdAt: moment(data.createdAt._seconds * 1000).format('DD/MM/YYYY'),
        updatedAt: moment(data.updatedAt._seconds * 1000).format('DD/MM/YYYY'),
      };
    });
  }

  async create(data) {
    const { email } = data.responsible;

    const userAlreadyExists = await usersRepository.getByKey('email', email);

    if (userAlreadyExists.length)
      throw new Error('O email do responsável já está em uso.');

    const user = await usersRepository.create({
      ...data.responsible,
      role: 1,
      active: null,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });

    await partnersRepository.create({
      ...data,
      active: null,
      responsible: {
        id: user.id,
        role: 1,
        active: true,
        ...data.responsible,
      },
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });
  }

  async findById(id) {
    const document = await partnersRepository.getById(id);

    if (!document) throw new NotFoundError('O documento não existe.');

    return {
      ...document,
      createdAt: moment(document.createdAt._seconds * 1000).format(
        'DD/MM/YYYY',
      ),
      updatedAt: moment(document.updatedAt._seconds * 1000).format(
        'DD/MM/YYYY',
      ),
    };
  }

  async updateById(id, data) {
    const document = await partnersRepository.getById(id);

    if (!document) throw new NotFoundError('O documento não existe.');

    if (Object.keys(data).indexOf('active') !== -3)
      await usersRepository.updateOrCreateById(document.responsible.id, {
        active: true,
      });

    await partnersRepository.updateOrCreateById(id, {
      ...data,
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });
  }

  async deleteById(id) {
    const document = await partnersRepository.getById(id);

    if (!document) throw new NotFoundError('O documento não existe.');

    partnersRepository.deleteById(id);
  }
}

module.exports = new PartnersService();
