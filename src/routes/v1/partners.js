const { Router } = require('express');

const partnersController = require('../../app/v1/controllers/partnersController');
const partnersValidator = require('../../app/v1/validators/partnersValidator');

const validateJoi = require('../../app/v1/libs/validateJoi');

const routes = new Router();

/**
 * @swagger
 * /partners:
 *   get:
 *     tags:
 *       - Partners
 *     description: Get all partners documents.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Sucess.
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/definitions/partnersValidator_createSchema_v1"
 *       400:
 *         description: Bad gateway.
 *       401:
 *         description: Error.
 *       404:
 *         description: Error.
 *       422:
 *         description: Error.
 *       500:
 *         description: Error.
 */
routes.get('/partners', [], partnersController.index);

/**
 * @swagger
 * /partners:
 *   post:
 *     tags:
 *       - Partners
 *     description: Store partners data.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: payload
 *         description: document data.
 *         schema:
 *           $ref: "#/definitions/partnersValidator_createSchema_v1"
 *     responses:
 *       201:
 *         description: Sucess, register created.
 *       400:
 *         description: Error.
 */
routes.post(
  '/partners',
  [validateJoi(partnersValidator.createSchema, 'body')],
  partnersController.store,
);

/**
 * @swagger
 * /partners/{id}:
 *   get:
 *     tags:
 *       - Partners
 *     description: Search partners data by id.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: document identification.
 *         in: path
 *     responses:
 *       200:
 *         description: Sucess.
 *         schema:
 *           $ref: "#/definitions/partnersValidator_createSchema_v1"
 *       400:
 *         description: Error.
 */
routes.get('/partners/:id', [], partnersController.show);

/**
 * @swagger
 * /partners/{id}:
 *   put:
 *     tags:
 *       - Partners
 *     description: Update partners data by id.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: document identification.
 *       - in: body
 *         name: payload
 *         description: document data.
 *         schema:
 *           $ref: "#/definitions/partnersValidator_updateSchema_v1"
 *     responses:
 *       204:
 *         description: Sucess, data updated.
 *       400:
 *         description: Error.
 */
routes.put(
  '/partners/:id',
  [validateJoi(partnersValidator.updateSchema, 'body')],
  partnersController.update,
);

/**
 * @swagger
 * /partners/{id}:
 *   delete:
 *     tags:
 *       - Partners
 *     description: Delete partners data by id.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: document identification.
 *     responses:
 *       204:
 *         description: Sucess, data updated.
 *       400:
 *         description: Error.
 */
routes.delete('/partners/:id', [], partnersController.destroy);

module.exports = routes;
