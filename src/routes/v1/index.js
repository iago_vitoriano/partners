const partners = require('./partners');
const posts = require('./posts');

module.exports = {
  partners,
  posts,
};
