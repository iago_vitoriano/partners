const { Router } = require('express');

const postsController = require('../../app/v1/controllers/postsController');
const postsValidator = require('../../app/v1/validators/postsValidator');

const routes = new Router();

const validateJoi = require('../../app/v1/libs/validateJoi');

/**
 * @swagger
 * /partners/{partnerId}/posts:
 *   get:
 *     tags:
 *       - Posts
 *     description: Get all posts documents.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Sucess.
 *         schema:
 *           type: array
 *           items:
 *             $ref: "#/definitions/postsValidator_createSchema_v1"
 *       400:
 *         description: Bad gateway.
 *       401:
 *         description: Error.
 *       404:
 *         description: Error.
 *       422:
 *         description: Error.
 *       500:
 *         description: Error.
 */
routes.get('/partners/:partnerId/posts', [], postsController.index);

/**
 * @swagger
 * /partners/{partnerId}/posts:
 *   post:
 *     tags:
 *       - Posts
 *     description: Store posts data.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: payload
 *         description: document data.
 *         schema:
 *           $ref: "#/definitions/postsValidator_createSchema_v1"
 *     responses:
 *       201:
 *         description: Sucess, register created.
 *       400:
 *         description: Error.
 */
routes.post(
  '/partners/:partnerId/posts',
  [validateJoi(postsValidator.createSchema, 'body')],
  postsController.store,
);

/**
 * @swagger
 * /partners/{partnerId}/posts/{id}:
 *   get:
 *     tags:
 *       - Posts
 *     description: Search posts data by id.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: document identification.
 *         in: path
 *     responses:
 *       200:
 *         description: Sucess.
 *         schema:
 *           $ref: "#/definitions/postsValidator_createSchema_v1"
 *       400:
 *         description: Error.
 */
routes.get('/partners/:partnerId/posts/:id', [], postsController.show);

/**
 * @swagger
 * /partners/{partnerId}/posts/{id}:
 *   put:
 *     tags:
 *       - Posts
 *     description: Update posts data by id.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: document identification.
 *       - in: body
 *         name: payload
 *         description: document data.
 *         schema:
 *           $ref: "#/definitions/postsValidator_updateSchema_v1"
 *     responses:
 *       204:
 *         description: Sucess, data updated.
 *       400:
 *         description: Error.
 */
routes.put(
  '/partners/:partnerId/posts/:id',
  [validateJoi(postsValidator.updateSchema, 'body')],
  postsController.update,
);

/**
 * @swagger
 * /partners/{partnerId}/posts/{id}:
 *   delete:
 *     tags:
 *       - Posts
 *     description: Delete posts data by id.
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: document identification.
 *     responses:
 *       204:
 *         description: Sucess, data updated.
 *       400:
 *         description: Error.
 */
routes.delete('/partners/:partnerId/posts/:id', [], postsController.destroy);

module.exports = routes;
