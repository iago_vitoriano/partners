const admin = require('firebase-admin');
const path = require('path');
const fs = require('fs');

if (process.env === 'restoque') {
  const accountService = JSON.parse(
    fs.readFileSync(
      path.resolve(
        __dirname,
        '..',
        '..',
        process.env.GOOGLE_APPLICATION_CREDENTIALS
      ),
      'utf-8'
    )
  );
  admin.initializeApp({ credential: admin.credential.cert(accountService) });
} else {
  admin.initializeApp();
}

const db = admin.firestore();

module.exports = db;
