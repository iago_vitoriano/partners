require('../config/bootstrap');

const path = require('path');
const config = require('config');

const swagger = require('../app/v1/libs/swagger');

(() => {
  const swaggerVersions = Object.keys(config.get('swagger'));

  swaggerVersions.forEach(version => {
    const validatorsPath = path.resolve(
      __dirname,
      '..',
      'app',
      version,
      'validators',
    );
    const swaggerPath = path.resolve(
      __dirname,
      '..',
      'app',
      version,
      'swagger',
    );

    swagger(validatorsPath, swaggerPath, version);
  });
})();
