const server = require('./app');

const port = process.env.PORT || 3000;

server.listen(port, () => {
  process.stdout.write(`Running partners on port ${port} \n`);
});
