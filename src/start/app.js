require('express-async-errors');
require('./database');
require('../config/bootstrap');

const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const config = require('config');
const helmet = require('helmet');
const cors = require('cors');

const globalErrorHandler = require('../app/v1/libs/globalErrorHandler');
const router = require('../routes');

class App {
  constructor() {
    this.init();
  }

  init() {
    this.server = express();

    this.middlewares();
    this.routes();
    this.swagger();
    this.exceptionHandler();
  }

  middlewares() {
    this.server.use(express.json());
    this.server.use(express.urlencoded({ extended: false }));
    this.server.use(helmet());
    this.server.use(cors());

    /*
    * EXPOSE STATIC FILES
    this.server.use(
      '/public',
      express.static(path.resolve(__dirname, '..', 'resources', 'public'))
    );
    */
  }

  routes() {
    const versions = Object.keys(router);

    versions.forEach(version => {
      const routes = Object.keys(router[version]);
      routes.forEach(route => {
        this.server.use(`/api/${version}`, router[version][route]);
      });
    });
  }

  swagger() {
    const swaggerConfig = config.get('swagger');
    const swaggerVersions = Object.keys(config.get('swagger'));

    swaggerVersions.forEach(version => {
      this.server.use(
        `/api/${version}/docs`,
        swaggerUi.serve,
        swaggerUi.setup(swaggerJSDoc(swaggerConfig[version])),
      );
    });
  }

  exceptionHandler() {
    this.server.use(globalErrorHandler);
  }
}

module.exports = new App().server;
