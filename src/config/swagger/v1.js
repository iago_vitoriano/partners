/* eslint-disable */

require('dotenv/config');

const path = require('path');
const directory = require('../../app/v1/libs/directory');

const deployEnvironment = process.env.DEPLOY_ENVIRONMENT
  ? process.env.DEPLOY_ENVIRONMENT.toLowerCase()
  : '';
let files;

try {
  files = directory
    .getDirectories(path.resolve(__dirname, '..', '..', 'app', 'v1', 'swagger'))
    .map(directories =>
      directory
        .getFiles(directories)
        .map(file => path.resolve(directories, file)),
    );
} catch (err) {
  files = [];
}

const getRoutes = sourcePath => {
  return Object.keys(require(sourcePath)).map(route => {
    return path.resolve(sourcePath, `${route}.js`);
  });
};

module.exports = {
  swaggerDefinition: {
    info: {
      title: 'partners',
      version: '1.0.0',
    },
    basePath: `${deployEnvironment === 'firebase' ? '/partners' : ''}/api/v1`,
    securityDefinitions: {
      jwt: {
        type: 'apiKey',
        name: 'Authorization',
        in: 'header',
      },
    },
    security: [{ jwt: [] }],
  },
  apis: [
    ...getRoutes(path.resolve(__dirname, '..', '..', 'routes', 'v1')),
    ...[].concat(...files),
  ],
};
