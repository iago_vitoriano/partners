# Restoque JavaScript Worbook() {

---

Um padrão enxuto para facilitar a manutenibilidade de grandes bases de código em longo prazo.

> **Nota**: Este documento explora as motivações e escolha do padrão de projeto adotado pelo PDX - Guia de estilo, nomenclatura de arquivos e diretórios, estrutura de diretórios, controle de versionamento, uso de bibliotecas internas, documentação e testes.

## Índice

---

1. [Bibliotecas](#bibliotecas)
1. [Guia de estilo](#guia-de-estilo)
1. [Convenções de nomenclatura](#convencoes-de-nomenclatura)
1. [Estrutura de diretórios](#estrutura-de-diretorios)
1. [Controle de versionamento](#controle-de-versionamento)
1. [Testes](#testes)
1. [Documentação](#documentacao)
1. [Pacotes internos](#pacotes-internos)
1. [Recursos](#recursos)

## Bibliotecas

---

- [1.1](#) Essa é a lista de bibliotecas e recursos utilizados para manter o padrão de projeto sem muitas dificuldades:

- [npm scopes](https://docs.npmjs.com/using-npm/scope.html)
- [editorconfig](https://editorconfig.org/)
- [eslint](https://eslint.org/)
- [prettier](https://prettier.io/)
- [dotenv](https://github.com/motdotla/dotenv)
- [jest](https://jestjs.io/)
- Carregando...

** [⬆ voltar ao topo](#índice) **

## Guia de estilo

---

- [2.1](#) O guia de estilo utilizado como base é o da [Airbnb](https://github.com/airbnb/javascript), mas com algumas variações que simplificam o mesmo.

** [⬆ voltar ao topo](#índice) **

## Convenções de nomenclatura

---

- [3.1](#) As convenções de nomenclatura adotadas são baseadas no padrão da Airbnb, mas com algumas variações:

- Nomeie seus funções, variáveis e classes de forma explícita.
- Use `camelCase` quando for nomear objetos, funções e instâncias.
- Use `PascalCase` apenas ao nomear construtores ou classes.
- Caso seu arquivo exporte apenas uma classe, objeto ou função seu nome deve ser o mesmo.
- Não use `sublinhados: \_` para identificar propriedades privadas, isso não existe no JavaScript.
- Não salve referências a `this`. Use as `arrow functions` ou a função `bind`.
- Acrônimos devem seguir o `PascalCase`, mas caso tenha `apenas duas letras`, ambas devem ser maiúsculas.

** [⬆ voltar ao topo](#índice) **

## Estrutura de diretórios

---

- [4.1](#) A estrutura básica normalmente se parece com isso:

```
.
│
└───src
│
│
│   .editorconfig
│   .env.example
│   .env.test
│   .eslintignore
│   .eslintrc.js
│   .gitconfig
│   .gitignore
│   .npmrc
│   jest.config.js
│   nodemon.json
│   package-lock.json
│   package.json
│   prettier.config.js
│   README.md
```

- [4.2](#) Uma visão geral da responsabilidade dos diretórios e arquivos que formam a base:

| Diretório/Arquivo  |                   Descrição                    |
| ------------------ | :--------------------------------------------: |
| src                |     Contém todo o código fonte do projeto      |
| app                |                Regra de negócio                |
| v1                 |          Armazena a v1 do webservice           |
| controllers        |   Abstrai os verbos HTTP e invoca o service    |
| helpers            |                  Utilitários                   |
| jobs               |                      Jobs                      |
| middlewares        |                   Middleware                   |
| repositories       |   Abstrai a interação com o modelo de dados    |
| services           | Processa os dados com base na regra de negócio |
| views              |             Telas para renderizar              |
| config             |          Configurações do webservice           |
| bootstrap.js       |         Setup básico de configurações          |
| default.js         |     Arquivo para servir o módulo "config"      |
| modules            |   Diretório para armazenar as configurações    |
| swagger.js         |        Configurações básicas do swagger        |
| resources          | Recursos do webservice (Jsons, arquivos, etc.) |
| public             |   Arquivos para serem servidos estáticamente   |
| routes             |               Diretório de rotas               |
| v1.js              |           Rotas da v1 do webservice            |
| index.js           |      Arquivo para exportar todas as rotas      |
| start              |       diretório com o core do webservice       |
| app.js             |               core do aplicativo               |
| .editorconfig      |                   descricao                    |
| .env.example       |                   descricao                    |
| .env.test          |                   descricao                    |
| .eslintignore      |                   descricao                    |
| .eslintrc.js       |                   descricao                    |
| .gitconfig         |                   descricao                    |
| .gitignore         |                   descricao                    |
| .npmrc             |                   descricao                    |
| jest.config.js     |                   descricao                    |
| nodemon.json       |                   descricao                    |
| package-lock.json  |                   descricao                    |
| package.json       |                   descricao                    |
| prettier.config.js |                   descricao                    |
| README.md          |                   descricao                    |

** [⬆ voltar ao topo](#índice) **

## Versionamento

---

O versionamento do projeto é baseado na pasta v1 que fica dentro de app.

** [⬆ voltar ao topo](#índice) **

## Testes

---

Os testes utilizados são baseados no Jest.

** [⬆ voltar ao topo](#índice) **

## Pacotes internos

---

Os escopos são incorporados ao npm e são uma maneira de agrupar pacotes. No Azure DevOps e no npmjs.com você pode publicar e usar pacotes com escopo e sem escopo.

- [8.1](#) Os escopos são o único recurso nativo do npm para usar vários registros. Eles permitem que você separe seus pacotes particulares dos pacotes npmjs.com prefixando-os com um `@scope:` - ex: `@restoque/pdx-helpers-lib`. Com isso, basta configurar um arquivo `.npmrc` para utilizar o registro da Azure ao invés do npmjs.com ao buscar este pacote.

** [⬆ voltar ao topo](#índice) **

## Recursos

---

**Leia isto**

- [Standard ECMA-262](http://www.ecma-international.org/ecma-262/6.0/index.html)
- [ES6 Features](https://github.com/lukehoban/es6features) - Luke Hoban

**Leituras interessantes**

- [Understanding JavaScript Closures](https://javascriptweblog.wordpress.com/2010/10/25/understanding-javascript-closures/) - Angus Croll
- [You Might Not Need jQuery](http://youmightnotneedjquery.com/) - Zack Bloom & Adam Schwartz

**Livros**

- [You Don't Know JS - 1nd Edition/PT-BR](https://github.com/cezaraugusto/You-Dont-Know-JS) - Kyle Simpson
- [You Don't Know JS - 2nd Edition](https://github.com/getify/You-Dont-Know-JS) - Kyle Simpson
- [Superhero.js](http://superherojs.com/) - Kim Joar Bekkelund, Mads Mobæk, & Olav Bjorkoy
- [Eloquent JavaScript](http://eloquentjavascript.net/) - Marijn Haverbeke

**Blogs**

- [JavaScript Weekly](http://javascriptweekly.com/)

** [⬆ voltar ao topo](#índice) **

# }

---
