/* eslint-disable global-require */
require('dotenv/config');

const deployEnvironment = process.env.DEPLOY_ENVIRONMENT.toLowerCase() || '';

switch (deployEnvironment) {
  case 'firebase':
    require('./src/start/firebase');
    break;
  case 'restoque':
    require('./src/start/restoque');
    break;
  default:
    throw new Error('Deploy environment is not defined.');
}
